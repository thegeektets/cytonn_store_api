import json

from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from transaction.models import Order, Order_Item, Payment
from transaction.serializers import PaymentSerializer, OrderSerializer, OrderItemSerializer


class OrderViewSet(viewsets.ModelViewSet):

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (AllowAny,)


class OrderItemViewSet(viewsets.ModelViewSet):

    queryset = Order_Item.objects.all()
    serializer_class = OrderItemSerializer
    permission_classes = (AllowAny,)


class PaymentViewSet(viewsets.ModelViewSet):

    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    permission_classes = (AllowAny,)


class CheckoutViewSet(viewsets.ViewSet):
    queryset = Payment.objects.all()
    permission_classes = (AllowAny,)


