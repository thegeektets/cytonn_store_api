from rest_framework import serializers
from .models import Product, Category, FileUpload

class FileUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileUpload

    datafile=serializers.CharField(read_only=True)


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = '__all__'

class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product

    datafile = FileUploadSerializer(many=False, read_only=True)
    datafile_id = serializers.IntegerField()






